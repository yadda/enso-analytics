<?php

namespace Yadda\Enso\Analytics\Controllers;

use Alert;
use Analytics;
use Illuminate\Routing\Controller;
use Log;
use Spatie\Analytics\Period;

class AnalyticsController extends Controller
{
    public function index()
    {
        if (!config('analytics.view_id')) {
            Alert::error('Analytics is currently unavailable.');
            Log::error(
                'Analytics page was accessed but there is no View ID available. ' .
                    'See Analytics setup instructions.'
            );
            return view('enso-crud::empty');
        }

        $most_visited_pages = Analytics::fetchMostVisitedPages(Period::days(7));
        $top_referrers = Analytics::fetchTopReferrers(Period::days(7));
        $user_types = Analytics::fetchUserTypes(Period::days(7));
        $top_browsers = Analytics::fetchTopBrowsers(Period::days(7));

        return view('enso-analytics::report', compact(
            'most_visited_pages',
            'top_referrers',
            'user_types',
            'top_browsers'
        ));
    }
}
