@extends(config('enso.crud.layout'))

@section('content')
    <div class="container is-fluid">
        <header class="is-clearfix">
            <h1 class="title is-1 is-pulled-left">Analytics</h1>
        </header>

        <hr>

        <h2 class="title is-2">Most visited pages</h2>
        <table class="table is-fullwidth">
          <thead>
            <th>URL</th>
            <th>Title</th>
            <th>Views</th>
          </thead>
          <tbody>
            @foreach($most_visited_pages as $page)
              <tr>
                <td>{{ $page['url'] }}</td>
                <td>{{ $page['pageTitle'] }}</td>
                <td>{{ $page['pageViews'] }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>

        <h2 class="title is-2">Top Referrers</h2>
        <table class="table is-fullwidth">
          <thead>
            <th>URL</th>
            <th>Page Views</th>
          </thead>
          <tbody>
            @foreach($top_referrers as $referrer)
              <tr>
                <td>{{ $referrer['url'] }}</td>
                <td>{{ $referrer['pageViews'] }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>

        <h2 class="title is-2">User Types</h2>
        <table class="table is-fullwidth">
          <thead>
            <th>Type</th>
            <th>Sessions</th>
          </thead>
          <tbody>
            @foreach($user_types as $type)
              <tr>
                <td>{{ $type['type'] }}</td>
                <td>{{ $type['sessions'] }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>

        <h2 class="title is-2">Top Browsers</h2>
        <table class="table is-fullwidth">
          <thead>
            <th>Type</th>
            <th>Sessions</th>
          </thead>
          <tbody>
            @foreach($top_browsers as $browser)
              <tr>
                <td>{{ $browser['browser'] }}</td>
                <td>{{ $browser['sessions'] }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
    </div>
@endsection
